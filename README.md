# Unity Subsystem

## Tujuan dan Fungsi Subsistem (Unity)

Fungsi dari subsistem ini merupakan komponen utama dari fungsionalitas sistem.
Subsistem ini memiliki tujuan untuk memberikan pengetahuan kepada pengguna mengenai pertanian dan menghibur pengguna. 

## Daftar Fitur

Susbsitem ini memiliki fitur sebagai berikut,

* Menanam Tanaman

![Menanam Tanaman](screenshot/menanam1.png)

![Menanam Tanaman](screenshot/menanam2.png)

![Menanam Tanaman](screenshot/menanam3.png)

![Menanam Tanaman](screenshot/menanam4.png)


* Sistem Otentikasi

![Login Page](screenshot/login.png)

* Di Rumah atau di kamar

![Save Tools](screenshot/in-home.png)

* Memiliki Cuaca sesuai Waktu

![Time Dependant](screenshot/time.png) 

* Menjual Hasil Tanaman dan Ternak

![Sell Work](screenshot/sell-work.png)

## Ide Dasar Game

Berawal dari kegemaran bermain game Harvest Moon yang terkenal saat masa kecil.
Pada dasarnya ingin mengembangkan permainan Harvest Moon yang grafis seperti konsol Playstation 1 menjadi berbasis 3D yang akan menambah pengalaman dan kesan bermain permainan ini.
Permainan ini akan mengimplementasi bagian-bagian seperti menanam tanaman, merawat ternak dan menyimpan progress permaianan.
Sosialisasi dengan penduduk sekitar merupakan pengembangan selanjutnya jika diinginkan dan waktu yang lebih panjang. 

## Gameplay

Pada dasarnya tidak ada target kemenangan pada permainan ini, namun akan diberikan popularity atau yang bisa disebutkan progress baik buruknya yang dilakukan oleh pemain.
Semakin terawat kebun dan ternak serta kondisi keuangan membaik akan mendapatkan popularity yang baik atau memiliki progress yang baik, berlaku sebaliknya jika kondisi uang semakin buruk, kondisi ternak memburuk akan menyebabkan popularity jelek.
Semakin buruk popularity akan dapat menyebabkan permainan gagal atau “kalah”.
Namun permainan tetap dilanjutkan dari kondisi terakhir permainan pada pemain.
Scoring dilakukan pada popularity. Nilai popularity dibatasi pada nilai -100 hingga 100.
Nilai popularity ini semakin negatif semakin buruk sedangkan semakin positif semakin baik.
Pengguna akan menggunakan papan ketik atau keyboard untuk menggerakan player pada permainan.

## Aset-Aset yang Digunakan

* Global

    * Standard Assets (from Unity)

* Login-Register Scene

    * Firebase Authentication (Firebase Unity SDK)
    
    Sumber : https://dl.google.com/firebase/sdk/unity/firebase_unity_sdk_3.0.1.zip 

* Home/Room Scene

    * Bed
    
    Sumber : https://www.assetstore.unity3d.com/en/#!/content/16294 
    
    * Basement Package
    
    Sumber : https://www.assetstore.unity3d.com/en/#!/content/66032 
    
    * Rusty Pack Model
    
    Sumber : https://www.assetstore.unity3d.com/en/#!/content/14412 

* Scene Farm
    
    * Day Night Sky Boxes

    Sumber : https://www.assetstore.unity3d.com/en/#!/content/32236 

    * Cartoon Farm Crops

    Sumber : https://www.assetstore.unity3d.com/en/#!/content/79777 

    * Fence

    Sumber : https://www.assetstore.unity3d.com/en/#!/content/71535 
    
    * Bhutan Farm House

    Sumber : https://www.assetstore.unity3d.com/en/#!/content/19360 
    
    * Plants Reed Cherry
    
    Sumber : https://www.assetstore.unity3d.com/en/#!/content/4049 
    
    * Photo Scan Toadstools
    
    Sumber : https://www.assetstore.unity3d.com/en/#!/content/70294 
    
    * Sound
    
    Sumber :
    
    https://www.sounddogs.com/previews/101/mp3/128804_SOUNDDOGS__st.mp3 (Footstep in the water)
    
    https://freesound.org/people/OwlStorm/sounds/151235/download/151235__owlstorm__grassy-footstep-4.wav (Footstep in the grass)
    
    http://www.orangefreesounds.com/wp-content/uploads/Zip/Farm-ambience.zip (Background Sound)

# About

1. Bervianto Leo P - 13514047
2. Ade Surya Ramadhani - 13514049
3. M. Az-zahid Adhitya Silparensi - 13514095