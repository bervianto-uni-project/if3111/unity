﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO.Ports;
using UnityEngine;

namespace AssemblyCSharp
{
	public class SerialPortLineReader
	{
		private SerialPort m_SerialPort = null;
		private Thread m_ReadLoop = null;
		private object m_LockHandle = new object();
		private List<string> m_Lines = new List<string>();

		public bool IsDataAvailable
		{
			get
			{
				lock (m_LockHandle)
				{
					return m_Lines.Count > 0;
				}
			}
		}

		public string ReadLine()
		{
			string tmp = null;
			lock (m_LockHandle)
			{
				if (m_Lines.Count > 0)
				{
					tmp = m_Lines[0];
					m_Lines.RemoveAt(0);
				}            
			}
			return tmp;
		}


		public SerialPortLineReader(SerialPort aSerialPort)
		{
			m_SerialPort = aSerialPort;
			m_ReadLoop = new Thread(_ThreadFunc);
			m_ReadLoop.IsBackground = true;
			m_ReadLoop.Start();
		}

		private void _ThreadFunc()
		{
			while (m_ReadLoop.ThreadState != ThreadState.StopRequested)
			{
				string tmp = m_SerialPort.ReadLine();
				Debug.Log (tmp);
				lock (m_LockHandle)
				{
					m_Lines.Add(tmp);
				}
			}
		}
	}
}

