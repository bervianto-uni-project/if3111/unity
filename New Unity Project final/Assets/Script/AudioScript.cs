﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class AudioScript : MonoBehaviour {
	public AudioClip walkSound;
	public AudioClip walkwaterSound;
	public AudioSource bgmSource;
	public AudioSource walkSource;
	public RigidbodyFirstPersonController rigidbodyFirstPersonController;
	public bool onWater;

	// Use this for initialization
	void Start () {
		rigidbodyFirstPersonController = gameObject.transform.parent.GetComponent<RigidbodyFirstPersonController> ();
		if (bgmSource != null) {
			bgmSource.enabled = true;
			bgmSource.Play ();
		}
		onWater = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (onWater) {
			if (Input.GetAxis("Horizontal") > 0  || Input.GetAxis ("Vertical") > 0) {
				if(!walkSource.isPlaying) 
					walkSource.PlayOneShot (walkwaterSound, 1.0f);
			}
		} else {
			if (rigidbodyFirstPersonController.Grounded) {
				if (Input.GetAxis("Horizontal") > 0  || Input.GetAxis ("Vertical") > 0) {
					if(!walkSource.isPlaying) 
						walkSource.PlayOneShot (walkSound, 1.0f);
				}
			}
		}
	}
}
