﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AssemblyCSharp;
using System;

public class OptionPanelBehaviour : MonoBehaviour {

	public bool plantbool;
	public bool destroy;
	public bool water;
	public bool harvest;
	public GameObject plantButton;
	public GameObject destroyButton;
	public GameObject waterButton;
	public GameObject harvestButton;
	public SlotScript plantScript;

	// Use this for initialization
	void Start () {
		plantbool = false;
		destroy = false;
		water = false;
		harvest = false;
		plantScript = null;
		plantButton = transform.GetChild (0).gameObject;
		destroyButton = transform.GetChild (2).gameObject;
		waterButton = transform.GetChild (1).gameObject;
		harvestButton = transform.GetChild (4).gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		if (plantScript != null) {
			checkPlotStatus ();
			setText ();
		}
		plantButton.SetActive (plantbool);
		destroyButton.SetActive (destroy);
		waterButton.SetActive (water);
		harvestButton.SetActive (harvest);
	}

	public void checkPlotStatus(){
		if (plantScript.plot.plant_id > 0) {
			plantbool = false;
			destroy = true;
			if (!plantScript.plot.plant_death) {
				water = plantScript.plot.isPlantThirsty ();
				harvest = plantScript.plot.isHarvest ();
			} else {
				water = false;
				harvest = false;
			}
		} else {
			plantbool = true;
			destroy = false;
			harvest = false;
			water = false;
		}
		if (PlayerStatusBehaviour.statuscharacter != null) {
			if (PlayerStatusBehaviour.statuscharacter.character_stamina < 0) {
				plantbool = false;
				destroy = false;
				harvest = false;
				water = false;
			}
		}
	}

	public void setText(){
		Text optionPanelText = transform.GetChild (3).GetChild (0).GetComponent<Text> ();
		if (plantScript.plot.plant_id > 0) {
			if (!plantScript.plot.plant_death) {
				string panelText = "- Plant Name : " + plantScript.plot.creature_name + " \n";
				TimeSpan span;
				if (plantScript.plot.plant_phase >= plantScript.harvestPhase) {
					span = plantScript.plot.tillHarvest ();
					if (span.TotalSeconds > 0)
						panelText += "- Till Harvest : " + string.Format ("{0:00}:{1:00}:{2:00}", span.Hours, span.Minutes, span.Seconds) + "\n";
				} else {
					span = plantScript.plot.tillGrown ();
					if (span.TotalSeconds > 0)
						panelText += "- Till Growth : " + string.Format ("{0:00}:{1:00}:{2:00}", span.Hours, span.Minutes, span.Seconds) + "\n";
				}
				span = plantScript.plot.tillThirsty ();
				if(span.TotalSeconds > 0)
					panelText +="- Till Watering : "+string.Format("{0:00}:{1:00}:{2:00}", span.Hours, span.Minutes, span.Seconds)+"\n";
				span = plantScript.plot.tillDeath ();
				if(span.TotalSeconds > 0)
					panelText += "- Till Plant Wither : " + string.Format("{0:00}:{1:00}:{2:00}", span.Hours, span.Minutes, span.Seconds) + "";
				optionPanelText.text = panelText;
			} else {
				optionPanelText.text = "Plant is Dead \nYou can only destroy it";
			}
		} else {
			optionPanelText.text = "There is No Plant Here \nYou can Plant a seed Here";
		}
	}
}
