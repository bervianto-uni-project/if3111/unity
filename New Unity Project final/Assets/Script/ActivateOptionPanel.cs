﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class ActivateOptionPanel : MonoBehaviour {

	public bool optionpanelon; 
	public bool helppanelon;
	public int plantstatus;
	public RigidbodyFirstPersonController FPS;
	public bool plantingPlant;
	public PlantPlayerBehaviour Player;
	GameObject optionpanel;
	GameObject helppanel;
	public bool rotate;
	public bool exithelpon;
	public bool scenechange;

	// Use this for initialization
	void Start () {
		optionpanelon = false;
		exithelpon = false;
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		FPS = player.GetComponent<RigidbodyFirstPersonController> ();
		Player = player.GetComponent<PlantPlayerBehaviour> ();
		optionpanel = transform.GetChild (2).gameObject;
		helppanel = transform.GetChild (3).gameObject;
		transform.GetChild (1).gameObject.GetComponent<StatusBehaviour> ().enabled = true;;
		plantingPlant = Player.doingsomething;
		rotate = true;
		scenechange = false;
	}
	
	// Update is called once per frame
	void Update () {
		optionpanel.SetActive (optionpanelon);
		helppanel.transform.GetChild (0).GetComponent<Text> ().text = "";
		if (optionpanelon) {
			helppanel.SetActive (true);
			helppanel.transform.GetChild (0).GetComponent<Text> ().text += "Press \"Q\" to Unlock/Lock Mouse ";
		} else {
			helppanel.SetActive (false);
		}
		if (PlayerStatusBehaviour.statuscharacter != null) {
			if (PlayerStatusBehaviour.statuscharacter.character_stamina < 0) {
				string enter = "";
				if (optionpanelon) {
					enter = "\n";
				}
				helppanel.SetActive (true);
				helppanel.transform.GetChild (0).GetComponent<Text> ().text +=enter+ "Stamina have spent, rest a little";
			}
		}
		if (exithelpon) {
			Debug.Log ("Hello");
			helppanel.SetActive (true);
			helppanel.transform.GetChild (0).GetComponent<Text> ().text = "Press \"ESC\" for Quitting the Application";
		} 
		if (scenechange) {
			helppanel.SetActive (true);
			helppanel.transform.GetChild (0).GetComponent<Text> ().text = "Press \"SPACE\" To Go To Another Scene";
		}
		if (PlayerStatusBehaviour.statuscharacter == null && !exithelpon && !optionpanelon && !scenechange) {
			helppanel.SetActive (false);
		}else{
			if ((!exithelpon && !optionpanelon && !scenechange)) {
				if (PlayerStatusBehaviour.statuscharacter.character_stamina > 0) {
					helppanel.SetActive (false);
				}
			}
		}
		plantingPlant = Player.doingsomething;
		if (optionpanelon) {
			if (Player.plant != null) {
				optionpanel.GetComponent<OptionPanelBehaviour> ().plantScript = Player.plant.GetComponent<SlotScript>();
			}
			if (Input.GetKeyDown(KeyCode.Q)) {
				rotate = !rotate;
			}
			Cursor.visible = true;
			if(!plantingPlant && !rotate)
				FPS.mouseLook.SetCursorLock(false);
			if (rotate) {
				Cursor.visible = false;
				FPS.mouseLook.SetCursorLock(true);
			}
		} else {
			FPS.mouseLook.SetCursorLock (true);
			rotate = true;
			if (optionpanel.GetComponent<OptionPanelBehaviour> ().plantScript != null) {
				optionpanel.GetComponent<OptionPanelBehaviour> ().plantScript = null;
			}
		}
	}

}
