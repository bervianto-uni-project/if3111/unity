﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	[System.Serializable]
	public struct Creature
	{
		public int creature_id;
		public string creature_name;
		public int creature_hunger_limit;
		public int creature_harvest_phase;
		public int creature_max_phase;
		public int creature_growth_time;
		public bool creature_type;
	}
}

