﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class Clock : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//gameObject.GetComponent<GUIText>().text = DateTime.Now.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.GetComponent<Text>().text = DateTime.Now.ToString("yyyy-MM-dd  HH:mm:ss");
	}
}
