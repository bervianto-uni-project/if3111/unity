﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitBehaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionStay(Collision Collision){
		if (Collision.gameObject.CompareTag ("Player")) {
			GameObject Canvas = GameObject.FindGameObjectWithTag ("PlayerCanvas");
			Canvas.GetComponent<ActivateOptionPanel> ().exithelpon = true;
			if (Input.GetKeyDown (KeyCode.Escape)) {
				Application.Quit ();
			}
		}
	}

	void OnCollisionExit(Collision Collision){
		if (Collision.gameObject.CompareTag ("Player")) {
			GameObject Canvas = GameObject.FindGameObjectWithTag ("PlayerCanvas");
			Canvas.GetComponent<ActivateOptionPanel> ().exithelpon = false;
		}
	}
}
